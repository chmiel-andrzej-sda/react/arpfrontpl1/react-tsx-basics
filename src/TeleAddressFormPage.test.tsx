import { mockUseParams } from './__mocks__/MockReactRouterDom';
import { shallow, ShallowWrapper } from 'enzyme';
import {
	personWithDataM,
	personWithoutDataK,
	personWithoutDataM,
	teleAddressData
} from './dataProviders/People';
import { TeleAddressFormPage } from './TeleAddressFormPage';

describe('TeleAddressFormPage', (): void => {
	it('renders without id', () => {
		// given
		const mockModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({});

		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithDataM]}
				onModify={mockModify}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(0);
	});

	it('renders with exceeded id', () => {
		// given
		const mockModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '30' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithDataM]}
				onModify={mockModify}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(0);
	});

	it('renders with teleaddress data', () => {
		// given
		const mockModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithDataM]}
				onModify={mockModify}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(0);
	});

	it('renders fully M', () => {
		// given
		const mockModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithoutDataM]}
				onModify={mockModify}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(0);
	});

	it('renders fully K', () => {
		// given
		const mockModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });

		// when
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithoutDataK]}
				onModify={mockModify}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(0);
	});

	it('handles data change', () => {
		// given
		const mockModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithoutDataK]}
				onModify={mockModify}
			/>
		);

		// when
		wrapper.find('.input-phone').simulate('change', teleAddressData.phone);
		wrapper.find('.input-email').simulate('change', teleAddressData.email);
		wrapper
			.find('.input-street')
			.simulate('change', teleAddressData.street);
		wrapper
			.find('.input-house-no')
			.simulate('change', `${teleAddressData.houseNo}`);
		wrapper
			.find('.input-flat-no')
			.simulate('change', `${teleAddressData.flatNo}`);
		wrapper.find('.input-city').simulate('change', teleAddressData.city);
		wrapper
			.find('.input-country')
			.simulate('change', teleAddressData.country);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(0);
	});

	it('handles form submit', () => {
		// given
		const mockModify: jest.Mock = jest.fn();
		const mockPreventDefault: jest.Mock = jest.fn();

		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithoutDataM]}
				onModify={mockModify}
			/>
		);

		// when
		wrapper.find('.form-data').simulate('submit', {
			preventDefault: mockPreventDefault
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(0);
		expect(mockPreventDefault).toHaveBeenCalledTimes(1);
	});

	it('handles empty data send', () => {
		// given
		const mockModify: jest.Mock = jest.fn();

		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithoutDataM]}
				onModify={mockModify}
			/>
		);

		// when
		wrapper.find('.button-send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(0);
	});

	it('handles data change', () => {
		// given
		const mockModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithoutDataM]}
				onModify={mockModify}
			/>
		);
		wrapper.find('.input-phone').simulate('change', teleAddressData.phone);
		wrapper.find('.input-email').simulate('change', teleAddressData.email);
		wrapper
			.find('.input-street')
			.simulate('change', teleAddressData.street);
		wrapper
			.find('.input-house-no')
			.simulate('change', `${teleAddressData.houseNo}`);
		wrapper
			.find('.input-flat-no')
			.simulate('change', `${teleAddressData.flatNo}`);
		wrapper.find('.input-city').simulate('change', teleAddressData.city);
		wrapper
			.find('.input-country')
			.simulate('change', teleAddressData.country);

		// when
		wrapper.find('.button-send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(1);
		expect(mockModify).toHaveBeenCalledWith(0, personWithDataM);
	});

	it('handles correct data after errors', () => {
		// given
		const mockModify: jest.Mock = jest.fn();
		mockUseParams.mockReturnValue({ id: '0' });
		const wrapper: ShallowWrapper = shallow(
			<TeleAddressFormPage
				people={[personWithoutDataM]}
				onModify={mockModify}
			/>
		);
		wrapper.find('.button-send').simulate('click');
		wrapper.find('.input-phone').simulate('change', teleAddressData.phone);
		wrapper.find('.input-email').simulate('change', teleAddressData.email);
		wrapper
			.find('.input-street')
			.simulate('change', teleAddressData.street);
		wrapper
			.find('.input-house-no')
			.simulate('change', `${teleAddressData.houseNo}`);
		wrapper
			.find('.input-flat-no')
			.simulate('change', `${teleAddressData.flatNo}`);
		wrapper.find('.input-city').simulate('change', teleAddressData.city);
		wrapper
			.find('.input-country')
			.simulate('change', teleAddressData.country);

		// when
		wrapper.find('.button-send').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockModify).toHaveBeenCalledTimes(1);
		expect(mockModify).toHaveBeenCalledWith(0, personWithDataM);
	});
});
