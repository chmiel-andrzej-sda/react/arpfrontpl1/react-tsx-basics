import { Button, TextField } from '@mui/material';
import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import axios, { AxiosResponse } from 'axios';
import { TextInput } from './TextInput';

interface APIUser {
	id: string;
	first_name: string;
	last_name: string;
}

interface APIResponse {
	total_pages: number;
	page: number;
	data: APIUser[];
}

export function Requestor(): JSX.Element {
	const [data, setData] = React.useState<APIUser[]>([]);
	const [maxPages, setMaxPages] = React.useState<number>(1);
	const [page, setPage] = React.useState<number>(1);
	const [name, setName] = React.useState<string>('');
	const [job, setJob] = React.useState<string>('');
	const [id, setId] = React.useState<number>(0);
	const [success, setSuccess] = React.useState<boolean>(false);

	React.useEffect((): void => {
		handleClick(1);
	}, []);

	function handleClick(page: number): void {
		axios
			.get(`https://reqres.in/api/users?page=${page}`)
			.then((response: AxiosResponse<APIResponse>): APIUser[] => {
				setMaxPages(response.data.total_pages);
				setPage(response.data.page);
				return response.data.data;
			})
			.then(setData);
	}

	function handleCreate(): void {
		axios
			.post(`https://reqres.in/api/users`, { name, job })
			.then((response: AxiosResponse): any => response.data)
			.then((response: any) => {
				setData([
					...data,
					{
						id: response.id,
						first_name: response.name,
						last_name: response.job
					}
				]);
				setName('');
				setJob('');
			});
	}

	function handleNameChange(value: string): void {
		setName(value);
	}

	function handleJobChange(value: string): void {
		setJob(value);
	}

	function handleIdChange(event: React.ChangeEvent<HTMLInputElement>): void {
		setId(parseInt(event.target.value || '0'));
	}

	function handleDelete(): void {
		setSuccess(false);
		axios
			.delete(`https://reqres.in/api/users/${id}`)
			.then((response: AxiosResponse<{}>): void => {
				if (response.status === 204) {
					setSuccess(true);
				}
			});
	}

	return (
		<div>
			{Array(maxPages)
				.fill(1)
				.map(
					(element: number, index: number): number => element + index
				)
				.map(
					(element: number): JSX.Element => (
						<Button
							className={`fetch-button-${element}`}
							key={element}
							onClick={() => handleClick(element)}
							color={element === page ? 'secondary' : 'primary'}
						>
							Pobierz dane {element}
						</Button>
					)
				)}
			{data.length !== 0 && (
				<TableContainer component={Paper}>
					<Table
						sx={{ minWidth: 650 }}
						aria-label='simple table'
					>
						<TableHead>
							<TableRow>
								<TableCell>id</TableCell>
								<TableCell>first name</TableCell>
								<TableCell>last name</TableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{data.map(
								(
									apiUser: APIUser,
									index: number
								): JSX.Element => (
									<TableRow key={index}>
										<TableCell>{apiUser.id}</TableCell>
										<TableCell>
											{apiUser.first_name}
										</TableCell>
										<TableCell>
											{apiUser.last_name}
										</TableCell>
									</TableRow>
								)
							)}
						</TableBody>
					</Table>
				</TableContainer>
			)}
			<TextInput
				label='Name'
				onChange={handleNameChange}
				value={name}
			/>
			<TextInput
				label='Job'
				onChange={handleJobChange}
				value={job}
			/>
			<Button onClick={handleCreate}>Dodaj</Button>
			<br />
			<br />
			<Button onClick={handleDelete}>Usuń</Button>
			<TextField
				color={success ? 'success' : undefined}
				type='number'
				onChange={handleIdChange}
				value={id}
			/>
		</div>
	);
}
