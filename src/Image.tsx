export interface ImageProps {
	src: string;
	alt: string;
}

export function Image(props: ImageProps): JSX.Element {
	return (
		<img
			src={props.src}
			alt={props.alt}
			width={480}
			height={640}
		/>
	);
}
