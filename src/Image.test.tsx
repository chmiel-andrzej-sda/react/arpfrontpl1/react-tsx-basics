import { shallow, ShallowWrapper } from 'enzyme';
import { Image } from './Image';

describe('Image', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<Image
				alt='alt'
				src='src'
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
