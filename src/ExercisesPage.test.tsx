import { shallow, ShallowWrapper } from 'enzyme';
import { ExercisesPage } from './ExercisesPage';

describe('ExercisesPage', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(<ExercisesPage />);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
