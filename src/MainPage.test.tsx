import { shallow, ShallowWrapper } from 'enzyme';
import { MainPage } from './MainPage';

describe('MainPage', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(<MainPage />);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
