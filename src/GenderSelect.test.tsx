import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { GenderSelect } from './GenderSelect';
import { Gender } from './PersonalDetails';

describe('GenderSelect', (): void => {
	it('renders', () => {
		// given
		const mockChange: jest.Mock = jest.fn();
		jest.spyOn(React, 'useId').mockReturnValue('test-id');

		// when
		const wrapper: ShallowWrapper = shallow(
			<GenderSelect
				genders={['K', 'M']}
				label='label'
				value='K'
				onChange={mockChange}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockChange).toHaveBeenCalledTimes(0);
	});

	it('handlec value change', () => {
		// given
		const mockChange: jest.Mock = jest.fn();
		jest.spyOn(React, 'useId').mockReturnValue('test-id');
		let value: Gender = 'K';
		const wrapper: ShallowWrapper = shallow(
			<GenderSelect
				genders={['K', 'M']}
				label='label'
				value={value}
				onChange={mockChange}
			/>
		);

		// when
		value = 'M';

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockChange).toHaveBeenCalledTimes(0);
	});

	it('handlec value change', () => {
		// given
		const mockChange: jest.Mock = jest.fn();
		jest.spyOn(React, 'useId').mockReturnValue('test-id');

		const wrapper: ShallowWrapper = shallow(
			<GenderSelect
				genders={['K', 'M']}
				label='label'
				value='K'
				onChange={mockChange}
			/>
		);

		// when
		wrapper.find('#test-id').simulate('change', {
			target: {
				value: 'M'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockChange).toHaveBeenCalledTimes(1);
		expect(mockChange).toHaveBeenCalledWith('M');
	});
});
