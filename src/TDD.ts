export function isLengthEven(array?: number[]): boolean {
    if (!array) {
        return false;
    }
    return array.length % 2 === 0;
}

export function isPalindromeArray(array?: number[]): boolean {
    if (!array) {
        return false;
    }
    return array.join() === array.reverse().join();
}

export function arraysMatch(source?: number[], input?: number[]): boolean {
    if (!source) {
        return false;
    }
    if (!input || input.length === 0) {
        return true;
    }
    return input.every((element: number): boolean => source.includes(element));
}
