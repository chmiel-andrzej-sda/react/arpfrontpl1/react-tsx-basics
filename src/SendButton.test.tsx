import { shallow, ShallowWrapper } from 'enzyme';
import { SendButton } from './SendButton';

describe('SendButton', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<SendButton onClick={(): void => undefined} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles click', () => {
		// given
		const mockClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<SendButton onClick={mockClick} />
		);

		// when
		wrapper.find('.send-button').simulate('click');

		// then
		expect(mockClick).toBeCalledTimes(1);
	});
});
