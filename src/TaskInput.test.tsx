import { shallow, ShallowWrapper } from 'enzyme';
import { TaskInput } from './TaskInput';

describe('TaskInput', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<TaskInput onAdd={(): void => undefined} />
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('changes value', () => {
		// given
		const wrapper: ShallowWrapper = shallow(
			<TaskInput onAdd={(): void => undefined} />
		);

		// when
		wrapper.find('#outlined-basic').simulate('change', {
			currentTarget: {
				value: 'dupa'
			}
		});

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handle click', () => {
		// given
		const mockAdd: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<TaskInput onAdd={mockAdd} />);
		wrapper.find('#outlined-basic').simulate('change', {
			currentTarget: {
				value: 'dupa'
			}
		});

		// when
		wrapper.find('.task-input-button-add').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockAdd).toBeCalledTimes(1);
		expect(mockAdd).toBeCalledWith({
			text: 'dupa',
			removed: false,
			created: expect.any(Date)
		});
	});

	it('handles any key down', () => {
		// given
		const mockAdd: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<TaskInput onAdd={mockAdd} />);

		// when
		wrapper.find('#outlined-basic').simulate('keyDown', {
			key: 'a'
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockAdd).toBeCalledTimes(0);
	});

	it('handles Enter key down', () => {
		// given
		const mockAdd: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(<TaskInput onAdd={mockAdd} />);
		wrapper.find('#outlined-basic').simulate('change', {
			currentTarget: {
				value: 'dupa'
			}
		});

		// when
		wrapper.find('#outlined-basic').simulate('keyDown', {
			key: 'Enter'
		});

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockAdd).toBeCalledTimes(1);
		expect(mockAdd).toBeCalledWith({
			text: 'dupa',
			removed: false,
			created: expect.any(Date)
		});
	});
});
