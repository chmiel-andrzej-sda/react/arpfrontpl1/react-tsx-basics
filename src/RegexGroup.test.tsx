import { shallow, ShallowWrapper } from 'enzyme';
import { RegexGroup } from './RegexGroup';

describe('RegexGroup', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(<RegexGroup />);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders', () => {
		// given
		const wrapper: ShallowWrapper = shallow(<RegexGroup />);

		// when
		wrapper
			.find('.regex-input-field')
			.simulate('change', new RegExp(/..+/));

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
