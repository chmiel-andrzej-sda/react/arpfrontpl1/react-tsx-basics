import { Image } from './Image';
import { PersonalDetails } from './PersonalDetails';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

export interface PersonalDetailsComponentProps {
	readonly personalDetails: PersonalDetails;
}

export function PersonalDetailsComponent(
	props: PersonalDetailsComponentProps
): JSX.Element {
	return (
		<>
			<TableContainer component={Paper}>
				<Table
					sx={{ minWidth: 650 }}
					aria-label='simple table'
				>
					<TableHead>
						<TableRow>
							<TableCell>first name</TableCell>
							<TableCell>middle name name</TableCell>
							<TableCell>last name</TableCell>
							<TableCell>pesel</TableCell>
							<TableCell rowSpan={2}>
								<Image
									alt='alt'
									src='src'
								/>
							</TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						<TableRow>
							<TableCell>
								{props.personalDetails.firstName}
							</TableCell>
							<TableCell>
								{props.personalDetails.middleName}
							</TableCell>
							<TableCell>
								{props.personalDetails.lastName}
							</TableCell>
							<TableCell>{props.personalDetails.pesel}</TableCell>
						</TableRow>
					</TableBody>
				</Table>
			</TableContainer>
		</>
	);
}
