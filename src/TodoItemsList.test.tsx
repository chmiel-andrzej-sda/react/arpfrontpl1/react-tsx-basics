import { shallow, ShallowWrapper } from 'enzyme';
import { TodoItemsList } from './TodoItemsList';

describe('TodoItemsList', (): void => {
	it('renders empty', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<TodoItemsList
				tasks={[]}
				onDeleteClick={(): void => undefined}
				onRemoveClick={(): void => undefined}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(
			<TodoItemsList
				tasks={[
					{
						text: 'test 1',
						removed: false,
						created: new Date(Date.UTC(2000, 1, 1, 5, 30, 20))
					},
					{
						text: 'test 2',
						removed: true,
						created: new Date(Date.UTC(2005, 5, 5, 5, 55, 55))
					}
				]}
				onDeleteClick={(): void => undefined}
				onRemoveClick={(): void => undefined}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('handles delete click', () => {
		// given
		const mockDeleteClick: jest.Mock = jest.fn();
		const mockRemoveClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TodoItemsList
				tasks={[
					{
						text: 'test 1',
						removed: false,
						created: new Date(Date.UTC(2000, 1, 1, 5, 30, 20))
					}
				]}
				onDeleteClick={mockDeleteClick}
				onRemoveClick={mockRemoveClick}
			/>
		);

		wrapper.find('.task-component-0').simulate('deleteClick', 0);

		// then
		expect(mockDeleteClick).toHaveBeenCalledTimes(1);
		expect(mockDeleteClick).toHaveBeenCalledWith(0);
		expect(mockRemoveClick).toHaveBeenCalledTimes(0);
	});

	it('handles remove click', () => {
		// given
		const mockDeleteClick: jest.Mock = jest.fn();
		const mockRemoveClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TodoItemsList
				tasks={[
					{
						text: 'test 1',
						removed: false,
						created: new Date(Date.UTC(2000, 1, 1, 5, 30, 20))
					}
				]}
				onDeleteClick={mockDeleteClick}
				onRemoveClick={mockRemoveClick}
			/>
		);

		wrapper.find('.task-component-0').simulate('removeClick', 0);

		// then
		expect(mockRemoveClick).toHaveBeenCalledTimes(1);
		expect(mockRemoveClick).toHaveBeenCalledWith(0);
		expect(mockDeleteClick).toHaveBeenCalledTimes(0);
	});
});
