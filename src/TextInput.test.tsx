import { shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { TextInput } from './TextInput';

describe('TextInput', (): void => {
	it('renders', () => {
		// given
		jest.spyOn(React, 'useId').mockReturnValue('test-id');

		// when
		const wrapper: ShallowWrapper = shallow(
			<TextInput
				onChange={(): void => undefined}
				label='test'
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders with full data', () => {
		// given
		jest.spyOn(React, 'useId').mockReturnValue('test-id');

		// when
		const wrapper: ShallowWrapper = shallow(
			<TextInput
				onChange={(): void => undefined}
				label='test'
				error
				maxLength={10}
				placeholder='type something'
				value='value'
				type='password'
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
	});

	it('renders', () => {
		// given
		jest.spyOn(React, 'useId').mockReturnValue('test-id');
		const mockChange: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<TextInput
				onChange={mockChange}
				label='test'
			/>
		);

		// when
		wrapper.find('.text-input-field').simulate('change', {
			currentTarget: {
				value: 'test'
			}
		});

		// then
		expect(mockChange).toBeCalledTimes(1);
		expect(mockChange).toBeCalledWith('test');
	});
});
