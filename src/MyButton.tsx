export interface MyButtonProps {
	readonly reverse?: boolean;
	readonly defaultLabel: string;
	readonly onClick: (value: number) => void;
	readonly value: number;
}

export function MyButton(props: MyButtonProps): JSX.Element {
	function handleClick(): void {
		if (props.reverse) {
			props.onClick(props.value - 1);
		} else {
			props.onClick(props.value + 1);
		}
	}

	return (
		<button
			className='button-button'
			onClick={handleClick}
		>
			{props.defaultLabel}
		</button>
	);
}
