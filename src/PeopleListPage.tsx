import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import { BackButton } from './BackButton';
import { Person } from './Person';
import { PersonalDetails } from './PersonalDetails';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

export interface PeopleListPageProps {
	people: Person[];
}

export function PeopleListPage(props: PeopleListPageProps): JSX.Element {
	return (
		<>
			<BackButton to='/' />
			<TableContainer component={Paper}>
				<Table
					sx={{ minWidth: 650 }}
					aria-label='simple table'
				>
					<TableHead>
						<TableRow>
							<TableCell>Lp.</TableCell>
							<TableCell>Imię</TableCell>
							<TableCell>Drugie imię</TableCell>
							<TableCell>Nazwisko</TableCell>
							<TableCell>Płeć</TableCell>
							<TableCell></TableCell>
						</TableRow>
					</TableHead>
					<TableBody>
						{props.people.map(
							(person: Person, index: number): JSX.Element => {
								const details: PersonalDetails =
									person.personalDetails;
								return (
									<TableRow key={index}>
										<TableCell>{index}</TableCell>
										<TableCell>
											{details.firstName}
										</TableCell>
										<TableCell>
											{details.middleName}
										</TableCell>
										<TableCell>
											{details.lastName}
										</TableCell>
										<TableCell>{details.gender}</TableCell>
										<TableCell>
											<Button
												size='small'
												component={Link}
												to={`/person/${index}`}
											>
												Więcej &gt;
											</Button>
										</TableCell>
									</TableRow>
								);
							}
						)}
					</TableBody>
				</Table>
			</TableContainer>
			<Button
				component={Link}
				to='/add'
			>
				DODAJ &gt;
			</Button>
		</>
	);
}
