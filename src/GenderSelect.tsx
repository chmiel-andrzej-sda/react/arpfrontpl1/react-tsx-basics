import React from 'react';
import { Gender } from './PersonalDetails';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';

export interface GenderSelectProps {
	genders: Gender[];
	value: string;
	onChange: (value: Gender) => void;
	label: string;
	className?: string;
}

export function GenderSelect(props: GenderSelectProps) {
	const id: string = React.useId();

	function handleSelectChange(event: SelectChangeEvent): void {
		props.onChange(event.target.value as Gender);
	}

	return (
		<FormControl fullWidth>
			<InputLabel id={`${id}-label`}>{props.label}</InputLabel>
			<Select
				labelId={`${id}-label`}
				id={id}
				value={props.value}
				label={props.label}
				onChange={handleSelectChange}
			>
				{props.genders.map(
					(gender: string, index: number): JSX.Element => {
						return (
							<MenuItem
								key={index}
								value={gender}
							>
								{gender}
							</MenuItem>
						);
					}
				)}
			</Select>
		</FormControl>
	);
}
