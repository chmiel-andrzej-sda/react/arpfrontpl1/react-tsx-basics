import { shallow, ShallowWrapper } from 'enzyme';
import { MyButton } from './MyButton';

describe('MyButton', (): void => {
	it('renders', () => {
		// when
		const mockClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<MyButton
				defaultLabel='default label'
				value={0}
				onClick={mockClick}
			/>
		);

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockClick).toHaveBeenCalledTimes(0);
	});

	it('increments', () => {
		// given
		const mockClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<MyButton
				defaultLabel='default label'
				value={0}
				onClick={mockClick}
			/>
		);

		// when
		wrapper.find('.button-button').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockClick).toHaveBeenCalledTimes(1);
		expect(mockClick).toHaveBeenCalledWith(1);
	});

	it('increments', () => {
		// given
		const mockClick: jest.Mock = jest.fn();
		const wrapper: ShallowWrapper = shallow(
			<MyButton
				defaultLabel='default label'
				value={8}
				onClick={mockClick}
				reverse
			/>
		);

		// when
		wrapper.find('.button-button').simulate('click');

		// then
		expect(wrapper).toMatchSnapshot();
		expect(mockClick).toHaveBeenCalledTimes(1);
		expect(mockClick).toHaveBeenCalledWith(7);
	});
});
