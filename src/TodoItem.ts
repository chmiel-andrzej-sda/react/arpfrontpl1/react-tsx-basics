export interface TodoItem {
	readonly text: string;
	readonly removed: boolean;
	readonly created: Date;
}
