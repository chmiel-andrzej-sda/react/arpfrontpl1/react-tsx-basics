import { shallow, ShallowWrapper } from 'enzyme';
import { BackButton } from './BackButton';

describe('BackButton', (): void => {
	it('renders', () => {
		// when
		const wrapper: ShallowWrapper = shallow(<BackButton to='/path' />);

		// then
		expect(wrapper).toMatchSnapshot();
	});
});
